from prefect import flow
import settings
import pandas as pd

def read_csv(filename):
    try:
        df = pd.read_csv(filename, header=None)
        return df
    except FileNotFoundError:
        print("File not found. Please provide a valid filename.")
        return None

def check_duplicates(df, record):
    # Convert record to a DataFrame for compatibility with DataFrame operations
    record_df = pd.DataFrame([record])
    # Check if any row in df is identical to record_df
    is_duplicate = df.append(record_df, ignore_index=True).duplicated().iloc[-1]
    return is_duplicate

def append_record_to_csv(filename, record):
    df = read_csv(filename)
    if df is not None:
        if not check_duplicates(df, record):
            # Append the new record
            new_record_df = pd.DataFrame([record])
            df = pd.concat([df, new_record_df], ignore_index=True)
            # Write the updated DataFrame to the CSV, overwriting the original file
            df.to_csv(filename, header=False, index=False)
            print("Record added successfully.")
        else:
            print("Record already exists and was not added.")

@flow(log_prints=True)
def symbol_to_tracker(symbol: str):
    append_record_to_csv(settings.stock_tracker_db, symbol)

if __name__ == "__main__":
    pass
