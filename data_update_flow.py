from prefect import flow
import os
import pandas as pd
import settings
import yfinance as yf
from datetime import datetime

# (function) Get list of symbols from database
def tracker_to_symbols(pass_tracker_csv):
    tracker_df = pd.read_csv(pass_tracker_csv, header=None)
    temp_list = []
    for index in tracker_df.index:
        temp_list.append(tracker_df[0][index])
    return temp_list

def yfinance_fetch_and_clean(symbol):
        obj = yf.Ticker(symbol)
        today_date = datetime.now().strftime('%Y-%m-%d')
        data = obj.history(start="1900-01-01", end=today_date, interval="1d")
        data.reset_index(inplace=True)
        data.drop(columns=['Dividends', 'Stock Splits'], axis=1, inplace=True)
        data['Date'] = pd.to_datetime(data['Date']).dt.date
        data["Date"] = pd.to_datetime(data["Date"])
        data['Open'] = data['Open'].round(2)
        data['High'] = data['High'].round(2)
        data['Low'] = data['Low'].round(2)
        data['Close'] = data['Close'].round(2)
        return data

def store_csv(d_frame, where_to_store, filename):
    d_frame.to_csv(os.path.join(where_to_store, filename+".csv"), mode='w', index=False)

@flow(log_prints=True)
def data_update_flow():

    # Delete all csv files in database
    print('Files deleted:')
    for filename in os.listdir(settings.stock_data_path):
        file_path = os.path.join(settings.stock_data_path, filename)
        os.remove(file_path)
        print(str(file_path))

    # Symbols to fetch
    symbols = tracker_to_symbols(settings.stock_tracker_db)

    # Store csv files
    if symbols:
        for symbol in symbols:
            symbol_df = yfinance_fetch_and_clean(symbol)
            store_csv(symbol_df, settings.stock_data_path, symbol)
            print(os.path.join(settings.stock_data_path, symbol, ".csv"))

if __name__ == "__main__":
    pass
