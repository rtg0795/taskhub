from data_update_flow import data_update_flow
from add_symbol_to_tracker import symbol_to_tracker
from prefect.deployments import Deployment
# from prefect.orion.schemas.schedules import CronSchedule

# schedule_1 = CronSchedule(cron="0 18 * * *", timezone="America/Los_Angeles", day_or=True)
# schedule_2 = CronSchedule(cron="0 18 * * *", timezone="America/Los_Angeles", day_or=True)


data_update_flow_dep = Deployment.build_from_flow(
        flow=data_update_flow,
        name="data_update_flow",
        # schedule=schedule_1,
        # infra_overrides={"env": {"PREFECT_LOGGING_LEVEL": "DEBUG"}},
        work_queue_name="default-agent-pool"
    )

symbol_to_tracker_dep = Deployment.build_from_flow(
        flow=symbol_to_tracker,
        name="symbol_to_tracker",
        # schedule=schedule_1,
        # infra_overrides={"env": {"PREFECT_LOGGING_LEVEL": "DEBUG"}},
        work_queue_name="default-agent-pool"
    )


if __name__ == "__main__":
    data_update_flow_dep.apply()
    symbol_to_tracker_dep.apply()
